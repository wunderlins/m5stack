# m5stack

<div>
  <img src="cad/render.png" height="200">
  <img src="pct/../pcb/view_bottom.jpg" height="200">
  <img src="pct/../pcb/view_top.jpg" height="200">

</div>

 ## Build

```bash
# first run, install board an libs
pio pkg update

# compile and upload
pio run --target upload
```

## Tasks

- [x] unit testing
- [ ] system initialisation
  - [x] logger
  - [x] system info  
    - [x] RTC
      - [x] NTP
    - [x] read config ini
  - [ ] sleep / powersave
  - [x] static ip address settings
  - [x] wifi wpa
  - [ ] wifi wpa2 enterprise
    - [ ] time initialisation (initial time)
- [ ] GUI
  - [ ] Modal Error messages
  - [ ] 

# M5 stack Software

- [Arduino Library](https://docs.m5stack.com/zh_CN/quick_start/m5core/arduino)
- [M5stack Core2](https://docs.m5stack.com/en/api/core2/system)
- [M5 Stack Updater](https://github.com/tobozo/M5Stack-SD-Updater)
- [Unity Test Framework](https://github.com/ThrowTheSwitch/Unity/blob/master/docs/UnityAssertionsReference.md)

# M5 Stack Hardware

- [KiCad Footprint](https://github.com/botanicfields/KiCad-Template-M5Stack)
- [13 pin 0.5mm ribbon connector](https://www.aliexpress.com/item/1251297455.html)
- [BMP3563 RTC](https://www.aliexpress.com/item/1005003356809532.html)
- [Molex 5051101392](https://www.digikey.ch/de/products/detail/molex/5051101392/5700461)
- [Molex 0545481371](https://www.digikey.ch/de/products/detail/molex/0545481371/3196996)

# ESP-32 Box

- [Github Repo](https://github.com/espressif/esp-box)
- [STEP Files of case](https://github.com/espressif/esp-box/tree/master/hardware)
- [PCB Schema V2.5](https://github.com/espressif/esp-box/raw/master/hardware/esp32_s3_box_v2.5/schematic/SCH_ESP32-S3-BOX_V2.5_20211011.pdf)

# Barcode Scanner

## Waveshare 13 pin Connector

Top View:

```
───╮
01 ┼╴ NC
02 ┼╴ 3V3
03 ┼╴ GND ───────────╮     ╭────                 
04 ┼╴ RXD (UART/3V3) ╰─────┼ GND                       
05 ┼╴ TXD (UART/3V3)      ╶┼ ID                    
06 ┼╴ D+  ─────────────────┼ D+                    
07 ┼╴ D-  ─────────────────┼ D-                  
08 ┼╴ NC                  ╶┼ VDC 5V              
09 ┼╴ NC                   ╰────                      
10 ┼╴ BUZ
11 ┼╴ NC
12 ┼───┤4.7kΩ├── 3V3
───╯
```

## ESP Serial connection

```
    ESP32       | Waveshare
SER2  | SoftSS  | 
------+---------+----------
G13   |   G27 <---> TXD
G14   |   G19 <---> RXD
```

- [Waveshare](doc/Datasheets/Waveshare_Barcode_Scanner_Module_User_Manual_EN.pdf)
- [SEN-18088](https://www.digikey.ch/de/products/detail/sparkfun-electronics/SEN-18088/14322716)
- [DYscan DE2120](https://www.alibaba.com/product-detail/Qr-code-1D-2D-Barcode-Scanner_60821146066.html) | [Datasheet](doc/Datasheets/DE2120.docx)
- [YK-E3000H](https://www.alibaba.com/product-detail/mini-OEM-Scan-engine-QR-2D_62237160660.html)
- [M2y](https://www.alibaba.com/product-detail/M3y-2D-QR-1D-Bar-Code_1600472533809.html) | [Protocol](doc/Datasheets/M3y%20protocol.docx) | [Integration Guide](doc/Datasheets/M3Y_Integration%20Guide.docx) | [User Manual](doc/Datasheets/M3Y_User%20Manual.docx)