//#include <Arduino.h>
#include "global.h"
#include "config.h"
#include "boot.h"
#include "net.h"
#include "display.h"
#include "scanner.h"
#include "speaker.h"

static const char* TAG = "main";
wifi_cfg_t *wifi_config = NULL;
sysinfo_t *sysinfo = NULL;

void fatal(const char *message) {
  LOGE("FATAL: %s", message);
  while(true);
}

void setup() {
  // Partition information, this is useful when updates/ota are in use
  const esp_partition_t *partition = esp_ota_get_running_partition();
  LOGI("Boot partition: %s, 0%X", partition->label, partition->address);
  const esp_partition_t *next = esp_ota_get_next_update_partition(partition);
  LOGI("Next partition: %s, 0%X", next->label, next->address);

  M5.begin(
    /* bool LCDEnable = true              */ true, 
    /* bool SDEnable = true               */ true, 
    /* bool SerialEnable = true           */ false, 
    /* bool I2CEnable = false             */ false, 
    /* mbus_mode_t mode = kMBusModeOutput */ kMBusModeOutput
  );
  //M5.Axp.SetSpkEnable(true);
  LOGD("== M5 Hardware initialized");
  
  // check for firmware updates
  config_setup();
  LOGD("== Config file initialized");

  // read network configuration
	wifi_config->hostname_prefix = config_get_str("wifi", "hostname_prefix");
	wifi_config->ssid            = config_get_str("wifi", "ssid");
  wifi_config->password        = config_get_str("wifi", "password");
	wifi_config->identity        = config_get_str("wifi", "identity");
  wifi_config->ca_pem          = config_get_str("wifi", "cert_ca");
  wifi_config->client_crt      = config_get_str("wifi", "cert_client");
  wifi_config->client_key      = config_get_str("wifi", "cert_client_key");

  // read network settings
  wifi_config->type            = config_get_str("network", "type");
  wifi_config->address         = config_get_str("network", "address");
  wifi_config->netmask         = config_get_str("network", "netmask");
  wifi_config->gateway         = config_get_str("network", "gateway");
  wifi_config->dns_nameservers = config_get_str("network", "dns-nameservers");

  // ntp servers
  wifi_config->ntp1            = config_get_str("ntp", "server1");
  wifi_config->ntp2            = config_get_str("ntp", "server2");

  // check for updates
  boot_setup();
  LOGD("== Firmware update checked");

  sysinfo = system_information();
  LOGD("Build time: %d", sysinfo->mac_str);
  wifi_config->mac = strdup(sysinfo->mac_str);

  // start application
  setup_display();

  // start network activity on second core
  LOGD("== Starting task net_setup()");
  delay(200);
  xTaskCreatePinnedToCore(
    net_setup,    // Function that should be called
    "Network controller",    // Name of the task (for debugging)
    16000,            // Stack size (bytes)
    wifi_config,      // Parameter to pass
    1,               // Task priority
    NULL,
    0 // core 0
  );

  // initialize baarcode scanner
  scanner_setup();

  // time for soem tunes
  speaker_setup();

}

//Button myButton(10, 10, 300, 50, false, "Click Me", {BLACK, WHITE, WHITE});

void loop() {
  // LOGD("New main loop iteration, M5.update()");
  //if (myButton.wasPressed()) Serial.print("* ");
}