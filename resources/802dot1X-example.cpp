#include <Arduino.h>
#include "WiFi.h"
#include "esp_wpa2.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "M5Core2.h"
#include "Wire.h"
//#include <Calibri_24px.h>

String hostname = "Scan2D";
const char* ssid = "Gandalf";
const char* name = "SERVICE_WS";
int counter = 0;

const char* ca_pem = "";
const char* client_crt = "";
const char* client_key = "";

RTC_TimeTypeDef RTCtime;
RTC_DateTypeDef RTCDate;

void setupTime(){

  RTCtime.Hours = 12; //Set the time.
  RTCtime.Minutes = 45;
  RTCtime.Seconds = 33;
  M5.Rtc.SetTime(&RTCtime); //and writes the set time to the real time clock.

  RTCDate.Year = 2022;  //Set the date.
  RTCDate.Month = 05;
  RTCDate.Date = 16;
  M5.Rtc.SetDate(&RTCDate);
}

void connectWifiStartup() {

	esp_log_level_set("*", ESP_LOG_VERBOSE);
  Serial.print("Connecting to network: ");
  Serial.println(ssid);
  WiFi.disconnect(true);
	WiFi.mode(WIFI_STA);
  WiFi.setHostname(hostname.c_str());

  //esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();

	esp_wifi_sta_wpa2_ent_set_identity((uint8_t *) name, strlen(name));
	esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *) ca_pem, strlen(ca_pem));
	
	esp_wifi_sta_wpa2_ent_set_cert_key(
	(uint8_t *) client_crt, strlen(client_crt),
	(uint8_t *) client_key, strlen(client_key),
	(uint8_t *) NULL, 0);

	//esp_wifi_sta_wpa2_ent_enable(&config);	
	esp_wifi_sta_wpa2_ent_enable();	

	WiFi.begin(ssid);

  //M5.Lcd.setFreeFont(&Calibri_24px);
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),180);

	while (WiFi.status() != WL_CONNECTED) {
		delay(200);
    if (counter % 5 == 0){
      M5.Lcd.fillRect((160 - (M5.Lcd.textWidth(".....")/2)),170,50,20,BLACK);
      M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),180);
      delay(200);
    }
    M5.Lcd.print(".");
		Serial.print(".");
		counter++;
		if (counter >= 50) { //after 5 seconds timeout and head on
        break;		
	    }
	}

  /*if (WiFi.status() == WL_CONNECTED) { //if we are connected to network
    counter = 0; //reset counter
    Serial.println("Wifi is connected with IP: ");
    Serial.println(WiFi.localIP());   //inform user about his IP address
  } else if (WiFi.status() != WL_CONNECTED) { //if we lost connection, retry
    WiFi.begin(ssid);
  }*/
}

void connectWifi() {

	esp_log_level_set("*", ESP_LOG_VERBOSE);
  Serial.print("Connecting to network: ");
  Serial.println(ssid);
  WiFi.disconnect(true);
	WiFi.mode(WIFI_STA);
  WiFi.setHostname(hostname.c_str());

  //esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();

	esp_wifi_sta_wpa2_ent_set_identity((uint8_t *) name, strlen(name));
	esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *) ca_pem, strlen(ca_pem));
	
	esp_wifi_sta_wpa2_ent_set_cert_key(
	(uint8_t *) client_crt, strlen(client_crt),
	(uint8_t *) client_key, strlen(client_key),
	(uint8_t *) NULL, 0);

	//esp_wifi_sta_wpa2_ent_enable(&config);	
	esp_wifi_sta_wpa2_ent_enable();	

	WiFi.begin(ssid);

  //M5.Lcd.setFreeFont(&Calibri_24px);
  M5.Lcd.setTextColor(BLACK);
  M5.Lcd.fillRect(0, 22, 320, 158, WHITE);
  M5.Lcd.setCursor((160 - (M5.Lcd.textWidth("Verbinde mit WiFi")/2)),110);
  M5.Lcd.print("Verbinde mit WiFi");
  M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),135);
  for (int iii=0; iii<5; iii++){
    M5.Lcd.print(".");
    delay(200);
  }
  delay(3000);

	while (WiFi.status() != WL_CONNECTED) {
		delay(200);
    Serial.print(".");
		counter++;
		if (counter >= 50) { //after 5 seconds timeout try reconnect
        break;		
	    }
	}

  /*if (WiFi.status() == WL_CONNECTED) { //if we are connected to network
    counter = 0; //reset counter
    Serial.println("Wifi is connected with IP: ");
    Serial.println(WiFi.localIP());   //inform user about his IP address
  } else if (WiFi.status() != WL_CONNECTED) { //if we lost connection, retry
    WiFi.begin(ssid);
  }*/
}

void checkWiFi() {

  extern uint8_t WLR[];
  extern uint8_t WLG[];

  if (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.drawJpg(WLR, 1175,2,2);
    connectWifi();
  }
  else{
    M5.Lcd.drawJpg(WLG, 1102,2,2);
  }
}
