#pragma once
//#include <ESP32-Chimera-Core.h>
#include <M5Core2.h>
#include "esp_log.h"

//   QuoteIdent turns an unquoted identifier
//   into a (quoted) string:
//      QuoteIdent(foo) -> "foo"
#define QuoteIdent(ident) #ident

/*
#define LOGI(...) ESP_LOGI(TAG, __VA_ARGS__); M5.Lcd.printf(__VA_ARGS__); M5.Lcd.printf("\n")
#define LOGW(...) ESP_LOGW(TAG, __VA_ARGS__); M5.Lcd.printf(__VA_ARGS__); M5.Lcd.printf("\n")
#define LOGE(...) ESP_LOGE(TAG, __VA_ARGS__); M5.Lcd.printf(__VA_ARGS__); M5.Lcd.printf("\n")
#define LOGD(...) ESP_LOGD(TAG, __VA_ARGS__); M5.Lcd.printf(__VA_ARGS__); M5.Lcd.printf("\n")
*/

#define LOGI(...) ESP_LOGI(TAG, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(TAG, __VA_ARGS__)
#define LOGE(...) ESP_LOGE(TAG, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(TAG, __VA_ARGS__)

typedef struct sysinfo_t {
    const char *build_time = NULL;
    uint8_t mac[6] = {0};
    char *mac_str = NULL;
    esp_chip_info_t chip_info;
} sysinfo_t;

typedef struct wifi_cfg_t {
    char *hostname_prefix;
    char *ssid;
    char *password;
    char *identity;
    char *ca_pem;
    char *client_crt;
    char *client_key;
    char *mac;

    // network parameter
    char *type;
    char *address;
    char *netmask;
    char *gateway;
    char *dns_nameservers;

    // ntp servers
    char *ntp1;
    char *ntp2;
} wifi_cfg_t;

extern wifi_cfg_t *wifi_config;
extern sysinfo_t *sysinfo;
extern void fatal(const char *message);