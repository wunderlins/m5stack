#include "config.h"

static const char* TAG = "config";
static const char *config_file = "/config.ini";

static IniFile ini(config_file);
static const size_t bufferLen = 256;
static char *buffer;
int iniError = IniFile::errorNoError;

String config_str_error_message(uint8_t e) {
  String message("");

  switch (e) {
  case IniFile::errorNoError:
    message.concat("no error");
    break;
  case IniFile::errorFileNotFound:
    message.concat("file not found");
    break;
  case IniFile::errorFileNotOpen:
    message.concat("file not open");
    break;
  case IniFile::errorBufferTooSmall:
    message.concat("buffer too small");
    break;
  case IniFile::errorSeekError:
    message.concat("seek error");
    break;
  case IniFile::errorSectionNotFound:
    message.concat("section not found");
    break;
  case IniFile::errorKeyNotFound:
    message.concat("key not found");
    break;
  case IniFile::errorEndOfFile:
    message.concat("end of file");
    break;
  case IniFile::errorUnknownError:
    message.concat("unknown error");
    break;
  default:
    message.concat("unknown error value");
    break;
  }

  return message;
}

bool config_open_ini_file() {
  LOGD("ini.open()");
  return ini.open();
}

void config_close_ini_file() {
  LOGD("ini.close()");
  return ini.close();
}

bool config_validate_file() {
  LOGD("ini.validate(buffer, %d)", bufferLen);
  return ini.validate(buffer, bufferLen);
}

void config_setup() {
  LOGI("config_setup(): %s", config_file);
  buffer = (char*) malloc(bufferLen);
  wifi_config = (wifi_cfg_t*) malloc(sizeof(wifi_cfg_t));

  if (!config_open_ini_file()) {
    LOGE("Ini file '%s' does not exist", config_file);
    while (1);
  }

  // Check the file is valid. This can be used to warn if any lines
  // are longer than the buffer.
  if (!config_validate_file()) {
    LOGE("Ini file '%s' not valid: %s", ini.getFilename(), config_str_error_message(ini.getError()));
    while (1);
  }

}

char* config_get_str(const char* section, const char* key) {
  LOGD("Reading [%s]->%s", section, key);
  iniError = IniFile::errorNoError;
  char *retval = (char*) malloc(100);
  retval[0] = 0;
  bool ret = ini.getValue(section, key, buffer, bufferLen, retval, 100);
  if (ret) {
    //LOGD("Found: %s", retval);
    return retval;
  }
  //LOGD("No value found, ret: %s", retval);
  return NULL;
}

/* untested
int config_get_int(const char* section, const char* key) {
  iniError = IniFile::errorNoError;
  if (ini.getValue(section, key, buffer, bufferLen))
    return atoi(buffer);
  iniError = ini.getError();
  return 0;
}
*/