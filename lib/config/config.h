#pragma once
#include "global.h"
#include <IniFile.h>

extern int iniError;

String config_str_error_message(uint8_t e);
void config_setup();
char* config_get_str(const char* section, const char* key);
//int config_get_int(const char* section, const char* key);
//bool config_start_sd();
bool config_open_ini_file();
bool config_validate_file();
