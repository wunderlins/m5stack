#pragma once
#include "global.h"

#define SDU_APP_NAME "M5Stack"
#define SDU_AUTHOR_NAME "Simon Wunderlin"
#define SDU_APP_PATH "/firmware.bin"

#include <M5StackUpdater.h>

#ifdef __cplusplus
extern "C" {
#endif

//static const int build_time = COMPILE_UNIX_TIME;
static const char build_time[] = __DATE__ " " __TIME__;

typedef struct datetime_t {
    RTC_TimeTypeDef *time;
    RTC_DateTypeDef *date;
} datetime_t;

void boot_setup();
sysinfo_t *system_information();
char *system_mac_str(sysinfo_t *sysinfo);
datetime_t *boot_date_time();
void boot_set_rtc(long unixts_utc);

#ifdef __cplusplus
}
#endif