#include "boot.h"
#include "WiFi.h"
#include "time.h"

static const char* TAG = "boot";

void boot_setup() {
  //system_information();

  LOGI("Checking for Updates ... ");

  SDUCfg.setAppName(SDU_APP_NAME);        // Lobby screen label: application name
  SDUCfg.setAuthorName(SDU_AUTHOR_NAME);  // lobby screen label: application author
  SDUCfg.setBinFileName(SDU_APP_PATH);    // If file path to bin is set for this app, it will be checked at boot and created if not exist

  if (SD.exists(SDU_APP_PATH)) {
    LOGI("Found %s, initializing updater ...", SDU_APP_PATH);
    checkSDUpdater(SD);
    LOGI("Updating from SD Card ...");
    updateFromFS(SD, SDU_APP_PATH);
    LOGI("Removing Firmware file");
    SD.remove(SDU_APP_PATH);
    LOGI("Rebooting ...");
    ESP.restart();
  }

  LOGI("No firmware.bin found, booting normal ...");
}

char *system_mac_str(sysinfo_t *sysinfo) {
  char *mac = (char*) malloc(19);
  LOGD("Creating mac string");
  sprintf(mac, "%02X %02X %02X %02X %02X %02X", 
    sysinfo->mac[0], sysinfo->mac[1], sysinfo->mac[2], 
    sysinfo->mac[3], sysinfo->mac[4], sysinfo->mac[5]);
  LOGD("mac_str: %s", mac);
  return mac;
}

sysinfo_t *system_information() {
  sysinfo_t *sysinfo = (sysinfo_t*) malloc(sizeof(sysinfo_t));
  sysinfo->mac_str = (char*) malloc(13);

  LOGD("Build Time: %s", build_time);
  sysinfo->build_time = build_time;

  //esp_chip_info_t chip_info;
  esp_chip_info(&sysinfo->chip_info);
  LOGD("Chip: %d, features: %d", sysinfo->chip_info.model, sysinfo->chip_info.features);

  // get system id, presumabliy same as MAC
  uint64_t mac = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t mac2 = (uint16_t)(mac >> 32);
  //LOGD("ESPID: %04X%08X", mac2, (uint32_t)mac);
  //return sysinfo;

  sysinfo->mac[5] = (uint8_t)(mac >> 0x0 );
  sysinfo->mac[4] = (uint8_t)(mac >> 0x8 );
  sysinfo->mac[3] = (uint8_t)(mac >> 0x10);
  sysinfo->mac[2] = (uint8_t)(mac >> 0x18);
  sysinfo->mac[1] = (uint8_t)(mac >> 0x20);
  sysinfo->mac[0] = (uint8_t)(mac >> 0x28);
  sprintf(sysinfo->mac_str, "%04X%08X", mac2, (uint32_t)mac);
  LOGD("MAC  : %s", sysinfo->mac_str);
 
  // get system time
  /*
  RTC_TimeTypeDef TimeStruct;
  RTC_DateTypeDef DateStruct;
  M5.Rtc.GetTime(&TimeStruct);
  M5.Rtc.GetDate(&DateStruct);
  datetime_t *dt = boot_date_time();

  LOGI("Date: %04d-%02d-%02dT%02d:%02d:%02d", dt->date->Year, dt->date->Month, dt->date->Date,
                                              dt->time->Hours, dt->time->Minutes, dt->time->Seconds); 
  */
  return sysinfo;
}

datetime_t *boot_date_time() {
  datetime_t *dt = (datetime_t*) malloc(sizeof(datetime_t));
  dt->time = (RTC_TimeTypeDef*) malloc(sizeof(RTC_TimeTypeDef));
  dt->date = (RTC_DateTypeDef*) malloc(sizeof(RTC_DateTypeDef));
  M5.Rtc.GetTime(dt->time);
  M5.Rtc.GetDate(dt->date);
  return dt;
}

void boot_set_rtc(long unixts_utc) {
  // set esp32 clock
  struct timeval now = { .tv_sec = unixts_utc};
  settimeofday(&now, NULL);

  // set RTC
  time_t t = (long) now.tv_sec;
  tm *time = gmtime (/* const time_t *_timer */ (long*) &unixts_utc);

  RTC_TimeTypeDef TimeStruct;
  TimeStruct.Hours   = time->tm_hour; //Set the specific time of the real-time clock structure.
  TimeStruct.Minutes = time->tm_min;
  TimeStruct.Seconds = time->tm_sec;

  RTC_DateTypeDef DateStruct;
  DateStruct.WeekDay = time->tm_wday;
  DateStruct.Month = time->tm_mon + 1; // starts at 0
  DateStruct.Date = time->tm_mday;
  DateStruct.Year = time->tm_year + 1900; // starts at 1900
  LOGD("Setting month, year: %d, %d", time->tm_mon, time->tm_year);

  //Writes the set time to the real-time clock.
  M5.Rtc.SetTime(&TimeStruct);
  M5.Rtc.SetDate(&DateStruct);
}
