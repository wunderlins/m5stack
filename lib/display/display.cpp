#include "display.h"
#include "scanner.h"
#include "speaker.h"

static const char* TAG = "display";

static void touched(Event& e) {
  LOGD("Touched: %s", e.button->getName());
}

void display_serve_gui(void * parameter) {

  Button BtnA = Button(3,   218, 102, 21, false, "Btn A", {BLACK, WHITE, WHITE}, {WHITE, BLACK, BLACK});
  Button BtnB = Button(109, 218, 102, 21, false, "Btn B", {BLACK, WHITE, WHITE}, {WHITE, BLACK, BLACK});
  Button BtnC = Button(215, 218, 102, 21, false, "Btn C", {BLACK, WHITE, WHITE}, {WHITE, BLACK, BLACK});

  BtnA.addHandler(touched, E_TOUCH);
  BtnB.addHandler(touched, E_TOUCH);
  BtnC.addHandler(touched, E_TOUCH);
  //myButton.addHandler(released, E_RELEASE);

  bool BtnA_state = false;
  bool BtnB_state = false;
  bool BtnC_state = false;

  M5.Lcd.setTextSize(1);

  while(true) {
    M5.update();

    // button events
    if (M5.BtnA.isPressed() && !BtnA_state) {
      BtnA_state = true;
      LOGD("BtnA_state true, .isPressed()");
    } else if (M5.BtnA.isReleased() && BtnA_state) {
      BtnA_state = false;
      LOGD("BtnA_state false, .isReleased()");
    }

    if (M5.BtnB.isPressed() && !BtnB_state) {
      BtnB_state = true;
      LOGD("BtnB_state true, .isPressed()");
    } else if (M5.BtnB.isReleased() && BtnB_state) {
      BtnB_state = false;
      LOGD("BtnB_state false, .isReleased()");
    }

    if (M5.BtnC.isPressed() && !BtnC_state) {
      BtnC_state = true;
      LOGD("BtnC_state true, .isPressed()");
    } else if (M5.BtnC.isReleased() && BtnC_state) {
      BtnC_state = false;
      LOGD("BtnC_state false, .isReleased()");
    }

    if (scanner_ready && xSemaphoreTake(scanner_data_mutex, 0) == pdTRUE ) {
      if (scanner_last_value->read == false) {
        LOGD("Scanner ready: %s ", (const char*) scanner_last_value->data);
        //M5.Lcd.print((const char*) scanner_last_value->data);
        M5.Lcd.setTextDatum(TL_DATUM);
        M5.Lcd.drawString((const char*) scanner_last_value->data, /* X/h */ 20, /* y/v */ 50, 2);
        scanner_last_value->read = true;
        M5.Axp.SetLDOEnable(3, true);
        M5.Axp.SetLDOEnable(3, false);
        DingDong();
      }
      xSemaphoreGive(scanner_data_mutex);
    }

  }

  vTaskDelete( NULL );
}

// Button myButton(10, 10, 300, 50, false, "Click Me", {BLACK, WHITE, WHITE});

void setup_display() {
  LOGI("Display: w: %d, h: %d", M5.lcd.width(), M5.lcd.height());

  xTaskCreatePinnedToCore(
    display_serve_gui,    // Function that should be called
    "Display controller",    // Name of the task (for debugging)
    4096,            // Stack size (bytes)
    NULL,            // Parameter to pass
    1,               // Task priority
    NULL,
    1 // core 1
  );
}


