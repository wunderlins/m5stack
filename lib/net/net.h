#pragma once
#include "global.h"
#include "time.h"
//#include "esp_wifi.h"
#include <NTPClient.h>

/*
typedef struct net_task_param_t {
    wifi_cfg_t *wifi_config;
    sysinfo_t *sysinfo;
} net_task_param_t;
*/

void net_setup(void * parameter);
IPAddress *str2ip(char *str);
char **split_string(char *str);

void net_wifi_connect_cert(wifi_cfg_t* wifi_config);
void net_wifi_connect_password(wifi_cfg_t* wifi_config);