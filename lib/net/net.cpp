#include "WiFi.h"
#include "esp_wpa2.h"
#include "esp_log.h"
   #include "esp_wifi.h"

#include "net.h"
#include "boot.h"

static const char* TAG = "net";
static char *hostname = NULL;

// certs
static char *ca_pem     = nullptr;
static char *client_crt = nullptr;
static char *client_key = nullptr;

// globals
static const char* ssid = nullptr;
static const char* identity = nullptr;
static int retry_counter = 0;
static const int max_retry_attempts = 50;

typedef enum wifi_type_t {
  UNKNOWN = 0,
  PASSWORD,
  CERTIFICATE
} wifi_type_t;

wifi_type_t wifi_type = UNKNOWN;

IPAddress *str2ip(char *str) {
  int ip[4] = {-1, -1, -1, -1};
  char buf[4] = {0};
  int char_c = 0;
  int buf_c = 0;
  int part_c = 0;
  int len = strlen(str);

  //LOGD("str: %s, len: %d", str, len);

  for(char_c = 0; char_c < len; char_c++) {
    //LOGD("char %02d %c, %s", char_c, str[char_c], buf);
    if (str[char_c] == '.') { // we have a dot
      //LOGD("ip part %d: %s", part_c, buf);
      ip[part_c] = atoi(buf);
      part_c++;

      buf[0] = 0; buf[1] = 0; buf[2] = 0; buf[3] = 0;
      buf_c = 0;

      continue;
    }

    buf[buf_c] = str[char_c];
    buf_c++;
  }
  //LOGD("ip part %d: %s", part_c, buf);
  if (buf_c)
    ip[part_c] = atoi(buf);

  if (ip[3] == -1) {
    LOGE("Incomplete ip Address");
    return NULL;
  }

  if (part_c == 3) {
    IPAddress *addr = new IPAddress(ip[0], ip[1], ip[2], ip[3]);
    return addr;
  } else {
    LOGE("Failed to convert string into ipv4 address.");
  }

  return nullptr;
}

char **split_string(char *str) {
  char **ret = (char**) calloc(10, sizeof(char)); // 10 elements
  ret[0] = NULL;
  // dns servers must be splitted by space (there might 
  // be more than one)
  int i = 0;
  const char delim[2] = " ";
  char *token = strtok(str, delim);
  while (token != NULL) {
    //LOGD( "--> %s", token);
    ret[i] = token;
    //LOGD("fetch");
    token = strtok(NULL, delim);
    if (i==9) {
      LOGE("Buffer overrun, aborting.");
      return NULL;
    }
    i++;
    //LOGD("next");
  }

  ret[i] = NULL;
  return ret;
}

void net_time_update(wifi_cfg_t* task_param) {
  LOGI("Updating time from %s", task_param->ntp1);
  WiFiUDP ntpUDP;
  NTPClient timeClient(ntpUDP, task_param->ntp1);
  timeClient.setTimeOffset(0); // make sure to use UTC time
  timeClient.begin();

  int err_count = 0;
  while (!timeClient.isTimeSet()) {
    bool ret = timeClient.forceUpdate();
    if (ret == false) err_count++;
    if (err_count > 10)
      break;
    delay(200);
  }

  // try ntp server number two
  if (!timeClient.isTimeSet() && task_param->ntp2 != NULL) {
    timeClient.setPoolServerName(task_param->ntp2);
    while (!timeClient.isTimeSet()) {
      bool ret = timeClient.forceUpdate();
      if (ret == false) err_count++;
      if (err_count > 10)
        break;
      delay(200);
    }
  }

  if (timeClient.isTimeSet()) {
    LOGI("NTP Time: %s %d", timeClient.getFormattedTime(), timeClient.getEpochTime());

    boot_set_rtc(timeClient.getEpochTime());
    delay(50);

    // get RTC time
    datetime_t *dt = boot_date_time();
    LOGI("RTC Date: %04d-%02d-%02dT%02d:%02d:%02d", 
      dt->date->Year, dt->date->Month, dt->date->Date,
      dt->time->Hours, dt->time->Minutes, dt->time->Seconds
    );

  } else
    LOGE("Failed to tge time from ntp sevrer %s %s", task_param->ntp1, task_param->ntp2);
  
  timeClient.end();
}

void net_wifi_config_read(wifi_cfg_t* task_param) {
  LOGD("net_wifi_config_read()");

  int l = strlen(task_param->mac) + 1;
  if (task_param->hostname_prefix != NULL) {
    l += strlen(task_param->hostname_prefix);
    hostname = (char*) malloc(l);
    hostname[0] = 0;
    strcat(hostname, task_param->hostname_prefix);
  } else {
    hostname = (char*) malloc(l);
    hostname[0] = 0;
  }

  strcat(hostname, task_param->mac);

  if (strcmp(hostname, "") == 0) {
    fatal("Empty hostname, aborting");
  }

  if (task_param->ssid == NULL) {
    fatal("Empty SSID, aborting");
  }
	
	if (hostname != NULL)               LOGI("hostname:    %s", hostname);
	if (task_param->ssid != NULL)       LOGI("ssid:        %s", task_param->ssid);
	if (task_param->password != NULL)   LOGI("password:    %s", task_param->password);
	if (task_param->identity != NULL)   LOGI("identity:    %s", task_param->identity);
	if (task_param->ca_pem != NULL)     LOGI("cert_ca:     %s", task_param->ca_pem);
	if (task_param->client_crt != NULL) LOGI("cert_client: %s", task_param->client_crt);
	if (task_param->client_key != NULL) LOGI("client_key:  %s", task_param->client_key);

  // check how we authenticate
  if (task_param->ssid != NULL && task_param->password != NULL) {
    wifi_type = PASSWORD;
    LOGI("WIFI type: PASSWORD");
  } else if (task_param->ssid != NULL &&
           task_param->ca_pem != NULL &&
           task_param->client_crt != NULL &&
           task_param->client_key) {
    wifi_type = CERTIFICATE;
    LOGI("WIFI type: CERTIFICATE");
  } else {
    fatal("Invalid WIFI configuration, check config.ini");
  }

  // check if we need to configure a static ip address
  if (task_param->type != NULL && strcmp(task_param->type, "static") == 0) {
    // check if we have all required parameter to configure 
    // a static network config
    if (task_param->address == NULL || 
        task_param->netmask == NULL || 
        task_param->gateway == NULL || 
        task_param->dns_nameservers == NULL) {
      fatal("Invalid network config, aborting ...");
    }

    LOGI("type:        %s", task_param->type);
    LOGI("address:     %s", task_param->address);
    LOGI("netmask:     %s", task_param->netmask);
    LOGI("gateway:     %s", task_param->gateway);
    LOGI("dns:         %s", task_param->dns_nameservers);

    IPAddress *address = str2ip(task_param->address);
    if (address == nullptr) { fatal("Invalid address"); }
    LOGD("address: %s", address->toString().c_str());

    IPAddress *netmask = str2ip(task_param->netmask);
    if (netmask == nullptr) { fatal("Invalid netmask"); }
    LOGD("netmask: %s", netmask->toString().c_str());

    IPAddress *gateway = str2ip(task_param->gateway);
    if (gateway == nullptr) { fatal("Invalid gateway"); }
    LOGD("gateway: %s", gateway->toString().c_str());

    // dns servers must be splitted by space (there might 
    // be more than one)
    int dns_count = 0;
    char **dns = split_string(task_param->dns_nameservers);
    for (; dns_count<10; dns_count++) {
      if (dns[dns_count] == NULL) break;
      LOGD("%d %s", dns_count, dns[dns_count]);
    }

    if (dns_count < 1) { fatal("Invalid dns Servers"); }
    IPAddress *dns1 = str2ip(dns[0]);
    if (dns1 == nullptr) { fatal("Invalid dns1 address"); }
    
    IPAddress *dns2 = nullptr;
    if (dns_count > 1) {
      dns2 = str2ip(dns[1]);
      if (dns2 == nullptr) { fatal("Invalid dns2 address"); }
    }

    // ok, we have all information, let's tra to set ip information
    if (WiFi.config(*address, *gateway, *netmask, *dns1, *dns2) == false) {
      fatal("Failed to set static ip configuration.");
    }

    LOGI("Finished setting up IP Configuration.");
  }

}

void net_setup(void * parameter) {
  LOGD("Setting up Network ...");

  wifi_cfg_t *task_param = (wifi_cfg_t*) parameter;
  LOGD("mac: %s", task_param->mac);

  // get configuration values
	net_wifi_config_read(task_param);
  LOGD("== Network configuration initialized");

  // try to conenct
  while (WiFi.status() != WL_CONNECTED) {
    LOGI("Connecting to Network %s", task_param->ssid);
    if (wifi_type == PASSWORD)
      net_wifi_connect_password(wifi_config);
    else
      net_wifi_connect_cert(wifi_config);
    
    delay(1000);

    // if we connected successfully, get time from ntp server
    if ((WiFi.status() != WL_CONNECTED)) {
      net_time_update(task_param);
    }

    delay(5000);
  }

  delay(50);
	vTaskDelete(NULL);
}

void net_wifi_connect_password(wifi_cfg_t* wifi_config) {
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_config->ssid, wifi_config->password);
  LOGI("Connecting to WiFi ...");
  while (WiFi.status() == WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  LOGI("Client IP: %s", WiFi.localIP().toString().c_str());
}

int net_read_cert(char *filename, uint8_t **buffer) {
  char f[strlen(filename)+1] ="/";
  strcat(f, filename); // add leading slash
  LOGI("loading %s", f);
  size_t size = 0;
  //if (SD.exists(f)) return -1;

  LOGD("Opening %s", f);
  File fd = SD.open(f);
  if (!fd) return -2;
  
  size = fd.size();
  *buffer = (uint8_t*) malloc(size+1);

  LOGD("Reading %s", f);
  int ret = fd.read(*buffer , size);
  if (ret < 0)
    return -3;
  
  (*buffer)[size] = 0;
  return (int) size;
}

void net_wifi_connect_cert(wifi_cfg_t* wifi_config) {
  // read certificates
  int ret = 0;
  if (ca_pem == nullptr) {
    LOGD("Loading Certificate ca_pem");
    ret = net_read_cert(wifi_config->ca_pem, (uint8_t**) &ca_pem);
    if (ret < 1) {
      LOGE("loading ca_pem returned %d", ret);
      fatal("Failed to laod ca_pem");
    }
  }
  
  if (client_crt == nullptr) {
    LOGD("Loading Certificate client_crt");
    ret = net_read_cert(wifi_config->client_crt, (uint8_t**) &client_crt);
    if (ret < 1) {
      LOGE("loading client_crt returned %d", ret);
      fatal("Failed to laod client_crt");
    }
  }
  
  if (client_key == nullptr) {
    LOGD("Loading Certificate client_key");
    ret = net_read_cert(wifi_config->client_key, (uint8_t**) &client_key);
    if (ret < 1) {
      LOGE("loading client_key returned %d", ret);
      fatal("Failed to laod client_key");
    }
  }
    
  // debug dump keys
  LOGD("ca_pem:\n%s\n", ca_pem);
  LOGD("client_crt:\n%s\n", client_crt);
  LOGD("client_key:\n%s\n", client_key);

  retry_counter = 0;
	//esp_log_level_set("*", ESP_LOG_VERBOSE);
  LOGI("Connecting to network: %s", wifi_config->ssid);
  WiFi.disconnect(true);
	//WiFi.mode(WIFI_STA);

  tcpip_adapter_init();
  esp_wifi_set_mode(WIFI_MODE_STA);
  //WiFi.setHostname(hostname);

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  esp_wifi_init(&cfg);

  //wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  //esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();

  LOGD("setting identity and cert");
  esp_wifi_sta_wpa2_ent_clear_identity();
  esp_wifi_sta_wpa2_ent_clear_username();
  esp_wifi_sta_wpa2_ent_clear_password();

	esp_wifi_sta_wpa2_ent_set_identity((uint8_t *) wifi_config->identity, strlen(wifi_config->identity));
	esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *) ca_pem, strlen(ca_pem));
	
  LOGD("esp_wifi_sta_wpa2_ent_set_cert_key()");
	esp_wifi_sta_wpa2_ent_set_cert_key(
    (uint8_t *) client_crt, strlen(client_crt),
    (uint8_t *) client_key, strlen(client_key),
    (uint8_t *) NULL, 0);

	//esp_wifi_sta_wpa2_ent_enable(&config);	
  LOGD("esp_wifi_sta_wpa2_ent_enable()");
	ret = esp_wifi_sta_wpa2_ent_enable();
  if (ret != ESP_OK) {
    LOGE("esp_wifi_sta_wpa2_ent_enable() returned %d", ret);
    fatal("Failed to initialize wpa enterprise");
  }

  LOGD("WiFi.begin()");
	//WiFi.begin(wifi_config->ssid);
  ESP_ERROR_CHECK( esp_wifi_start() );
  esp_err_t r = tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, hostname);

  //M5.Lcd.setFreeFont(&Calibri_24px);
  //M5.Lcd.setTextColor(WHITE);
  //M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),180);

	while (WiFi.status() != WL_CONNECTED) {
		delay(200);
    if (retry_counter % 5 == 0){
      //M5.Lcd.fillRect((160 - (M5.Lcd.textWidth(".....")/2)),170,50,20,BLACK);
      //M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),180);
      delay(200);
    }
    //M5.Lcd.print(".");
		Serial.print(".");
		retry_counter++;
		if (retry_counter >= max_retry_attempts) { //after 5 seconds timeout and head on
      LOGE("Failed to connect ot %s, giving up after %d attempts", ssid, max_retry_attempts);
      return;
    }
	}

  LOGI("Connected to %s", wifi_config->ssid);

  /*if (WiFi.status() == WL_CONNECTED) { //if we are connected to network
    counter = 0; //reset counter
    Serial.println("Wifi is connected with IP: ");
    Serial.println(WiFi.localIP());   //inform user about his IP address
  } else if (WiFi.status() != WL_CONNECTED) { //if we lost connection, retry
    WiFi.begin(wifi_ssid);
  }*/
}

/*

void connectWifi() {

	esp_log_level_set("*", ESP_LOG_VERBOSE);
  Serial.print("Connecting to network: ");
  Serial.println(ssid);
  WiFi.disconnect(true);
	WiFi.mode(WIFI_STA);
  WiFi.setHostname(hostname.c_str());

  //esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT();

	esp_wifi_sta_wpa2_ent_set_identity((uint8_t *) name, strlen(name));
	esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *) ca_pem, strlen(ca_pem));
	
	esp_wifi_sta_wpa2_ent_set_cert_key(
	(uint8_t *) client_crt, strlen(client_crt),
	(uint8_t *) client_key, strlen(client_key),
	(uint8_t *) NULL, 0);

	//esp_wifi_sta_wpa2_ent_enable(&config);	
	esp_wifi_sta_wpa2_ent_enable();	

	WiFi.begin(ssid);

  //M5.Lcd.setFreeFont(&Calibri_24px);
  M5.Lcd.setTextColor(BLACK);
  M5.Lcd.fillRect(0, 22, 320, 158, WHITE);
  M5.Lcd.setCursor((160 - (M5.Lcd.textWidth("Verbinde mit WiFi")/2)),110);
  M5.Lcd.print("Verbinde mit WiFi");
  M5.Lcd.setCursor((160 - (M5.Lcd.textWidth(".....")/2)),135);
  for (int iii=0; iii<5; iii++){
    M5.Lcd.print(".");
    delay(200);
  }
  delay(3000);

	while (WiFi.status() != WL_CONNECTED) {
		delay(200);
    Serial.print(".");
		counter++;
		if (counter >= 50) { //after 5 seconds timeout try reconnect
        break;		
	    }
	}

  /*if (WiFi.status() == WL_CONNECTED) { //if we are connected to network
    counter = 0; //reset counter
    Serial.println("Wifi is connected with IP: ");
    Serial.println(WiFi.localIP());   //inform user about his IP address
  } else if (WiFi.status() != WL_CONNECTED) { //if we lost connection, retry
    WiFi.begin(ssid);
  }* /
}

void checkWiFi() {

  extern uint8_t WLR[];
  extern uint8_t WLG[];

  if (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.drawJpg(WLR, 1175,2,2);
    connectWifi();
  }
  else{
    M5.Lcd.drawJpg(WLG, 1102,2,2);
  }
}
*/