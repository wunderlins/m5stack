#pragma once
#include "global.h"

#define RXD2 13
#define TXD2 14

#define RXD3 27 /* 13 */
#define TXD3 19 /* 28 */

typedef struct scanner_result_t {
  union head {
    uint16_t head;
    uint8_t byte[2];
  } head;
  uint8_t type; // 0x00 read success
  uint8_t length; // length of the data section, 0x00 == 256
  uint8_t data[256];
  union crc {
    uint16_t crc;
    uint8_t byte[2];
  } crc;
} scanner_result_t;

typedef struct scanner_last_value_t{
  uint8_t data[256] = {0};
  uint8_t length = 0;
  bool read = false;
} scanner_last_value_t;

extern scanner_last_value_t *scanner_last_value;
extern SemaphoreHandle_t scanner_data_mutex;
extern bool scanner_ready;
void scanner_setup();
void scanner_service(void * parameter);
