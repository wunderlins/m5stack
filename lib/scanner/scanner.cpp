#include "scanner.h"
//#include <SoftwareSerial.h>

bool scanner_ready = false;
scanner_last_value_t *scanner_last_value = NULL;
SemaphoreHandle_t scanner_data_mutex = NULL;
static const char* TAG = "scanner";

static const int input_buffer_size = 100;
static char input_buffer[input_buffer_size] = {0};
static char input_buffer_hex[input_buffer_size*7+1] = {0};
//SoftwareSerial BCScannerSerial(RXD3, TXD3);
HardwareSerial BCScannerSerial(2);

void scanner_setup() {
  scanner_last_value = (scanner_last_value_t*) malloc(sizeof(scanner_last_value_t));
  scanner_data_mutex = xSemaphoreCreateMutex();

  // initialize state storage
  xSemaphoreTake( scanner_data_mutex, portMAX_DELAY );
  memcpy(scanner_last_value->data, "", 1);
  scanner_last_value->length = 0;
  scanner_last_value->read = false;
  xSemaphoreGive( scanner_data_mutex );

  scanner_ready = false;
  xTaskCreatePinnedToCore(
    scanner_service,    // Function that should be called
    "Scanner controller",    // Name of the task (for debugging)
    4096,            // Stack size (bytes)
    NULL,            // Parameter to pass
    2,               // Task priority
    NULL,
    1 // core 1
  );
}

typedef struct scanner_message_t {
  uint8_t head[2] = {0x7E, 0x00};
  uint8_t type    = 0x07;
  union address {
    uint16_t address;
    uint8_t  bytes[2];
  };
  uint8_t length = 0x00; // 0x00 means 256 bytes, there is no zero.
  uint8_t data[256] = {0}; 
  union crc {
    uint16_t crc;
    uint8_t  bytes[2]; // don't check, set: 0xAB 0xCD
  };
} scanner_message_t;

void scanner_read_mode() {

}

void print_result(scanner_result_t *res) {
  
  char buffer[1024] = {0};
  //LOGD("Data: %s", buffer); return;

  sprintf(buffer, "{head: %02X%02X, type: %02X, length: %02X, data: ", 
    res->head.byte[0], res->head.byte[1], res->type, res->length);
  
  for (int i=0; i<res->length; i++) {
    char buf[04] = {0};
    sprintf(buf, "%02X ", res->data[i]);
    strcat(buffer, buf);
  }

  strcat(buffer, ", crc: ");
  char crc[5] = {0};
  sprintf(crc, "%02X%02X", res->crc.byte[0], res->crc.byte[1]);
  strcat(buffer, crc);
  strcat(buffer, "}");

  LOGD("Data: %s", buffer);
}

void scanner_service(void * parameter) {
  delay(2500); // wait for network do come up
  LOGD("== initializing barcode Scanner");

  int i=0;

  // setup message
  /*
  byte message[] = {0x7E, 0x00, 0x08, 0x01, 0x00, 0x02, 0x01, 0xAB, 0xCD };
  long baud[7] = {1200, 4800, 9600, 14400, 19200, 38400, 57600};
  for (i=0; i<7; i++) {
    LOGD("trying: %d", baud[i]);
    // initialize UART2
    BCScannerSerial.begin(baud[i]);
    delay(50);
    //Query the baud rate
    byte message[] = {0x7E, 0x00, 0x07, 0x01, 0x00, 0x2A, 0x02, 0xD8, 0x0F };
    BCScannerSerial.write(message, 9);
    delay(50);
    BCScannerSerial.end();
    delay(50);
  }
  */

  LOGD("BCScannerSerial.begin()");
  BCScannerSerial.begin(9600, SERIAL_8N1, RXD2, TXD2, false);
  //Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2, false);
  delay(300);


  // Query the baud rate
  //byte message[] = {0x7E, 0x00, 0x07, 0x01, 0x00, 0x2A, 0x02, 0xD8, 0x0F };
  // enable uart + all barcodes
  byte message[] = {0x80, 0x30, 0x30, 0x30, 0x30, 0x30, 0x39, 0x30, 0x2E, 0x0D};
  // byte message[] = {0x7E, 0x00, 0x08, 0x01, 0x00, 0x0A, 0x3E, 0x4C, 0xCF };
  // send mode
  //byte message[] = {0x7E, 0x00, 0x08, 0x01, 0x00, 0x02, 0x01, 0xAB, 0xCD };
  BCScannerSerial.write(message, sizeof(message));

  // read response
  /*
  LOGD("BCScannerSerial.available()");
  while (BCScannerSerial.available() == 0) {delay(20);}
  while (BCScannerSerial.available() > 0) {
    Serial.printf("0x%02X ", BCScannerSerial.read());
  }
  Serial.printf("\n");
  */

  while(true) {
    if (BCScannerSerial.available() == 0) {
      delay(50);
      continue;
    }

    scanner_ready = true;
    int input_count = 0;
    int available = BCScannerSerial.available();
    char *hex_pos = (char*) &input_buffer_hex;
    scanner_result_t result = {.head = 0, .type = 0, .length = 0, .crc = 0};
    result.length = 0;
    while (input_count < available && input_count < input_buffer_size) {
      char c = BCScannerSerial.read();
      //Serial.printf("   0x%02X ", c);
      sprintf(hex_pos, "0x%02X, ", c); hex_pos += 6;
      input_buffer[input_count] = c;

      /*
      if (input_count == 0) result.head.byte[0] = c;
      else if (input_count == 1) result.head.byte[1] = c;
      else if (input_count == 2) result.type = c;
      else if (input_count == 3) result.length = c;
      else if (input_count > 3) {
        // we know the length
        if (input_count >= 4 && input_count < result.length+4) {
          result.data[input_count-4] = c;
          //LOGD("data c: %d", input_count);
        } else if(input_count == result.length+4) { 
          result.data[input_count-4] = 0; // delimit - so it can be used as *char
          result.crc.byte[0] = c;
        } else if(input_count == result.length+5) { 
          result.crc.byte[1] = c;
        } 
      }
      */
      
      input_count++;
    }
    input_buffer[input_count] = 0; // terminate string

    xSemaphoreTake( scanner_data_mutex, portMAX_DELAY );
    memcpy(scanner_last_value->data, input_buffer, input_count);
    scanner_last_value->length = input_count-1;
    scanner_last_value->data[input_count] = 0;
    scanner_last_value->read = false;
    xSemaphoreGive( scanner_data_mutex );

    //LOGI("Scanner data: %s", result.data);

    //LOGD("data[0] %02X", result.data[0]);
    //print_result(&result);

    LOGI("Scanner: '%s' %s"  , input_buffer, input_buffer_hex);
    delay(50);
  }

  delay(50);
  LOGE("Shutting down scanner process");
	vTaskDelete(NULL);

}

