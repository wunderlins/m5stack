#include "config.h"
#include "common.h"

wifi_cfg_t *wifi_config = NULL;
sysinfo_t *sysinfo = NULL;

void testing_logger(const char* str) {
    M5.Lcd.printf(str);
}

void hardware_init() {
  M5.begin(
    /* bool LCDEnable = true              */ true, 
    /* bool SDEnable = true               */ true, 
    /* bool SerialEnable = true           */ true, 
    /* bool I2CEnable = false             */ false, 
    /* mbus_mode_t mode = kMBusModeOutput */ kMBusModeOutput
  );

  // Wait ~2 seconds before the Unity test runner
  // establishes connection with a board Serial interface
  delay(2000);
}