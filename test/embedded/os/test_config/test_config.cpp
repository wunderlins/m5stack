#include "unity.h"
#include "common.h"
#include "config.h"
#include "net.h"

static const char* TAG = "test_config";

void setUp(void) {
  // set stuff up here
}

void tearDown(void) {
  // clean stuff up here
}

void test_config_init(void) {
  testing_logger("Testing SD Card ...\n");
  config_setup();
}

void test_config_read(void) {
  testing_logger("Reading network->type ...\n");
  char *type = config_get_str("network", "type");
  TEST_ASSERT_EQUAL_STRING("static", type);

  // read undefined value, should return NULL
  testing_logger("Reading whatever->key ...\n");
  char *undef = config_get_str("whatever", "key");
  TEST_ASSERT_NULL(undef);
}

void test_config_str_to_ip() {
  testing_logger("ip 10.0.0.1\n");
  char *ip1str = (char*) "10.0.0.1";
  IPAddress *ip1 = str2ip(ip1str);
  TEST_ASSERT_EQUAL_STRING(ip1str, ip1->toString().c_str());

  testing_logger("ip 255.255.255.255\n");
  char *ip2str = (char*) "255.255.255.255";
  IPAddress *ip2 = str2ip(ip2str);
  TEST_ASSERT_EQUAL_STRING(ip2str, ip2->toString().c_str());

  testing_logger("ip 255.255.255\n");
  char *ip3str = (char*) "255.255.255";
  IPAddress *ip3 = str2ip(ip3str);
  TEST_ASSERT_NULL(ip3);

  testing_logger("ip 255.255.255.\n");
  char *ip4str = (char*) "255.255.255.";
  IPAddress *ip4 = str2ip(ip4str);
  TEST_ASSERT_NULL(ip4);

  testing_logger("ip ''\n");
  char *ip5str = (char*) "";
  IPAddress *ip5 = str2ip(ip5str);
  TEST_ASSERT_NULL(ip5);

  testing_logger("ip 'abcd'\n");
  char *ip6str = (char*) "";
  IPAddress *ip6 = str2ip(ip6str);
  TEST_ASSERT_NULL(ip6);
}

void test_split_string() {
  char *str;
  char **ret;
  int len = 0;

  testing_logger("split '8.8.8.8 8.8.9.9'\n");
  str = (char*) "8.8.8.8 8.8.9.9";
  ret = split_string(str);
  TEST_ASSERT_NOT_NULL(ret);
  len = 0; while(ret[len++] != NULL);
  TEST_ASSERT_EQUAL_DOUBLE(2, len);
  TEST_ASSERT_EQUAL_STRING("8.8.9.9", ret[1]);

  return;

  testing_logger("split '8.8.8.8'\n");
  str = (char*) "8.8.8.8";
  ret = split_string(str);
  TEST_ASSERT_NOT_NULL(ret);
  len = 0; while(ret[len++] != NULL);
  TEST_ASSERT_EQUAL_DOUBLE(1, len);
  TEST_ASSERT_EQUAL_STRING("8.8.8.8", ret[0]);

}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_config_init);
  RUN_TEST(test_config_read);
  RUN_TEST(test_config_str_to_ip);
  //RUN_TEST(test_split_string);
  return UNITY_END();
}

void setup() {
  // intialize M5
  hardware_init();

  // run tests
  runUnityTests();
}

// unused
void loop() {}
