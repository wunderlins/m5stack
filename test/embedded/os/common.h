#pragma once

/**
 * @brief log test name to lcd screen
 * 
 * @param str 
 */
void testing_logger(const char* str);

/**
 * @brief initialize M5 hardware
 * 
 */
void hardware_init();