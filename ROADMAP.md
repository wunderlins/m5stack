# Development Roadmap

## Abstract

### Data collection

This Project shall provide functionality to add and manage items to a 
todo list. These items can be added from various sources:
 - Web Interface
 - Smart Phone
 - Handheld IoT Device

Additionally one automated data source should be added
 - Automated item addition thorugh Mirth Connect EAI System

The three user interfaces provide various benefits and 
in required effort for production and maintenance 
drawbacks. Also, there is a quite significant difference 
(Maintenance effort for the three solutions is not explicitly 
listed in this Document).

### Data Management

The Web Application shall provide a list of newly collected 
items which are enriched with data from an external sources 
collected trough Mirth Connect. These items shall be 
presented as a list with quick access links to addition 
applications (item context shall be provided to these 
applications). Additionally, status changes, notes and follow 
up actions shall be possible per item.

<div style="page-break-after: always;"></div>

## Project Component Breakdown

```plantuml
@startuml

package "Infrastructure" {
    [Backend Database] <..> DB
    DB <..> [Backend Service]
    [Mirth Connect] <..> DB
}

REST -down-> [Backend Service]
package "Web" {
  [Web User Interface] --> REST
  [List] --> [Web User Interface]
  [Add]  --> [Web User Interface]
  [List] --> [App 1]
  [List] --> [App 2]
  [List] --> [App 3]
}
[Mobile User Interface] -up-> REST

package "Embedded System" {
    package "Operating System" {
        [Core]
        [Storage] <--> [Core]
        [Config] <--> [Storage]
        [Updater] <--> [Storage]
        [Wifi] <--> [Storage]
        [NTP] <--> [Wifi]
        [Logging] <--> [Storage]
        [Network Communication]  <--> REST
    }

    [Embedded User Interface] <--> [Core]
    [Wifi] <--> [Network Communication]

    package "Production Hardware" {
        [Laser Scanner] <-up-> [Core]
        [3D Printed Case]
        [Custom PCB]
    }
}

@enduml
```
<div style="page-break-after: always;"></div>

## Project Dependencies

```plantuml
@startuml

<style>
ganttDiagram {
  task {
    BackGroundColor #888
    'LineColor Green 
    unstarted {
      BackGroundColor #AAA
      'LineColor FireBrick
    }
  }
}
</style>

Project starts the 01 of June 2022
saturday are closed
sunday are closed

[System Design] lasts 3 days

-- Backend Application --
[Software Design] lasts 1 days
[Software Design] starts at [System Design]'s end

[Backend Database] lasts 1 days
[Backend Database] starts at [Software Design]'s end

-- Mirth Connect --
[EAI Design] lasts 2 days
[EAI Design] starts at [Backend Database]'s end

[ADT] lasts 5 days
[ADT] starts at [EAI Design]'s end
[RIS ORR] lasts 5 days
[RIS ORR] starts at [EAI Design]'s end
[Diagnosis] lasts 5 days
[Diagnosis] starts at [EAI Design]'s end

-- Web Application --

[Backend Service] lasts 2 days
[Backend Service] starts at [ADT]'s end

[Web User Interface] lasts 3 days
[Web User Interface] starts at [Backend Service]'s end

[List] lasts 1 days
[List] starts at [Backend Service]'s end
[Manage] lasts 2 days
[Manage] starts at [List]'s end


-- Mobile Application --
[Mobile User Interface] lasts 2 days
[Mobile User Interface] starts at [Backend Service]'s end

-- Embedded Software --

[Storage] lasts 1 days
[Storage] starts at [System Design]'s end
[Updater] lasts 3 days
[Updater] starts at [Storage]'s end
[Wifi] lasts 5 days
[Wifi] starts at [Updater]'s end

[Laser Scanner] lasts 2 days
[Laser Scanner] starts at [Wifi]'s end

[Embedded User Interface] lasts 4 days
[Embedded User Interface] starts at [Wifi]'s end
[Embedded User Interface] starts at [Backend Service]'s end

[Network Communication] lasts 4 days
[Network Communication] starts at [Embedded User Interface]'s end

-- Embedded Hardware --

[Hardware Design] lasts 3 days
[Hardware Design] starts at [System Design]'s end

[Custom PCB Production] lasts 28 days
[Custom PCB Production] starts at [Hardware Design]'s end
[3D Printed Case Prototype] lasts 5 days
[3D Printed Case Prototype] starts at [Hardware Design]'s end

[3D Printed Case Production] lasts 28 days
[3D Printed Case Production] starts at [3D Printed Case Prototype]'s end

[Hardware Design] lasts 3 days
[Hardware Design] starts at [System Design]'s end

''' Progress '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

[System Design] is 30% complete

' Backend Application
[Software Design] is 0% complete
[Backend Database]  is 0% complete


' Mirth Connect
[EAI Design]  is 33% complete
[ADT] is 70% complete
[RIS ORR] is 0% complete
[Diagnosis] is 0% complete


' Web Application
[Backend Service] is 0% complete
[Web User Interface] is 0% complete
[List] is 0% complete
[Manage] is 0% complete

' Mobile Application
[Mobile User Interface] is 0% complete

' Embedded software 
[Storage] is 80% complete
[Updater] is 90% complete
[Wifi] is 25% complete
[Laser Scanner] is 0% complete
[Embedded User Interface] is 0% complete
[Network Communication] is 0% complete

' Embedded Hardware
[Hardware Design] is 10% complete
[Custom PCB Production] is 0% complete
[3D Printed Case Prototype] is 0% complete
[3D Printed Case Production] is 0% complete



@enduml
```
<div style="page-break-after: always;"></div>

## Open Questions

### Core
- [ ] which system
  - [ ] https://github.com/m5stack/M5Unified
  - [ ] https://github.com/m5stack/M5Core2
  - [ ] https://github.com/tobozo/ESP32-Chimera-Core

### Storage
- [ ] How to Flash for production?

### Config
- [ ] File format, ini/json/yaml
- [ ] Evaluate parser performance and memory usage

### Updater
- [ ] how to handle firmware updates?
  - [ ] https://github.com/tobozo/M5Stack-SD-Updater

### Wifi
- [ ] wpa config
- [ ] wpa2 enterprise config
- [ ] compatibility with wpa3 ?

### NTP
- [ ] preserve time after shutdown
- [ ] fallback when wpa fails, ask user for date ?
  - [ ] UI

### Logging
- [ ] make debug loggign configurable for trouble shooting

### Embedded User Interface
- [ ] gui/touch library ... prolly M5something

### Production Hardware
- [ ] production time
- [ ] production pricing
- [ ] ribbon cable connector (13 lanes, FPC 0.5mm) -> https://www.mouser.ch/ProductDetail/Molex/52892-1333?qs=s7UCm7gO1ban%2FHF1v5aKZQ%3D%3D
- [ ] 3D Design case

#### Laser Scanner
- [ ] find smaller module
- [ ] reverse enginner communication on Wavershare Module 13pin ribbon cable
#### Custom PCB
- [ ] find footprint for conenctor
- [ ] possible ttl shifter (5/3V3) ?
- [ ] buzzer maybe (maybe use speaker in m5)?