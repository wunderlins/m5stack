# M5 Core2 Adaptor board

This is an adaptor bor for the [M5Stack Core2][1] to [Waveshare Barcode Scanner][2].

A voltage divider from 5V to 3.3V is included (Scanner uses 3V3). I connects 
the M5 with an 2.54mm pitch header to a FCM/FPM 0.5mm ribbon cable.

<div style="clear: both;">
    <img src="view_top.jpg" style="float: left; width: 200px;">
    <img src="view_bottom.jpg" style="float: left; width: 200px;">
    <div style="clear: both"></div>
</div>

---

![](schema.jpg)

[1]: https://docs.m5stack.com/en/core/core2
[2]: https://www.waveshare.com/barcode-scanner-module.htm